import Life
import random

from Grass import Grass
from Rock import Rock
from Sven import Sven
from Water import Water
from Wolf import Wolf

width = 80
height = 60

size_world = [width, height]

p_wolf = 5
p_sven = 5
p_river = 10
p_rock = 3
p_grass = 40
p_upper_border = 1000

wolf_food_start = 1
wolf_food_end = 9
sven_food_start = 1
sven_food_end = 9


def can_event(p):
    if random.randint(0, p_upper_border) <= p:
        return True
    else:
        return False


def add_cell(x_cell, y_cell, h_cell, cell):
    for yl_wolf in range(0, h_cell // 2):
        for xl_wolf in range(0, h_cell // 2 - yl_wolf):
            add_object_on_map(cell(x_cell + xl_wolf, y_cell + yl_wolf, random.randint(wolf_food_start, wolf_food_end)))
            add_object_on_map(cell(x_cell - xl_wolf, y_cell - yl_wolf, random.randint(wolf_food_start, wolf_food_end)))
            add_object_on_map(cell(x_cell + xl_wolf, y_cell - yl_wolf, random.randint(wolf_food_start, wolf_food_end)))
            add_object_on_map(cell(x_cell - xl_wolf, y_cell + yl_wolf, random.randint(wolf_food_start, wolf_food_end)))


def move_river(x_cell, y_cell, dist, cell):
    if dist == 0:
        return
    x_offset = random.randint(-1, 1)
    y_offset = random.randint(-1, 1)
    add_object_on_map(cell(x_cell, y_cell))
    move_river(x_cell + x_offset, y_cell + y_offset, dist - 1, cell)


def add_object_on_map(cell):
    if not Life.obj_exist(cell.x, cell.y):
        Life.worldMap.append(cell)


def gen_new_map():
    Life.worldMap.clear()
    for x in range(0, size_world[0]):
        for y in range(0, size_world[1]):
            nEvent = random.randint(0, 4)
            if nEvent == 0 and can_event(p_wolf):
                radius = random.randint(4, 8)
                add_cell(x, y, radius, Wolf)
            elif nEvent == 1 and can_event(p_sven):
                radius = random.randint(4, 8)
                add_cell(x, y, radius, Sven)
            elif nEvent == 2 and can_event(p_river):
                move_river(x, y, random.randint(10, 30), Water)
            elif nEvent == 3 and can_event(p_rock):
                move_river(x, y, random.randint(50, 100), Rock)
            elif nEvent == 4 and can_event(p_grass):
                radius = random.randint(4, 16)
                add_cell(x, y, radius, Grass)

gen_new_map()