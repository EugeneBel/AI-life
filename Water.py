import Life
from Cell import Cell

class Water(Cell):

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.health = -1

    def update(self, tick):
        Life.newWorldMap.append(Water(self.x, self.y))

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getName(self):
        return 'w'

    def getHealth(self):
        return self.health