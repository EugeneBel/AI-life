import GeneratorWorld
import Life

import Rules

tick = 1
life = 1
fitness_wolf = 0
fitness_sven = 0

Rules.load_rules()

record_file = open('replay', 'w')
buffer_file = ""
run = True

while run:
    if not Life.life(tick):
        Rules.fitness_sven_offset = 0
        Rules.fitness_wolf_offset = 0
        Rules.stage_end()
        # run = False
        GeneratorWorld.gen_new_map()
        tick = 0
        life += 1
    tick += 1

    fitness_wolf = Rules.get_fitness(Rules.rules_wolf, Rules.fitness_wolf_offset)
    fitness_sven = Rules.get_fitness(Rules.rules_sven, Rules.fitness_sven_offset)

    print("life: " + str(life) + "  tick: " + str(tick) + "  fitness - w: "
          + str(fitness_wolf) + " s: " + str(fitness_sven))

    to_flush = ""
    for x in Life.worldMap:
        to_flush += str(x.getName()) + ":" + str(x.getX()) + ":" + str(x.getY()) + ","
    to_flush += "\n"
    record_file.write(to_flush)

Rules.stage_end()
record_file.close()