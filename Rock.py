import Life
from Cell import Cell

class Rock(Cell):

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.health = -1

    def update(self, tick):
        Life.newWorldMap.append(Rock(self.x, self.y))

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getName(self):
        return 'r'

    def getHealth(self):
        return self.health