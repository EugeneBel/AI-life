import random

import GeneratorWorld
import Life
import Rules
from Cell import Cell

class Wolf(Cell):

    def __init__(self, x, y, health):
        self.x = x
        self.y = y
        self.health = health

    def update(self, tick):

        if self.health < 0:
            Rules.fitness_wolf_offset -= 1
            return None

        action = Rules.get_action(self.x, self.y, 'wolf')
        action = int(action)
        x = 0
        y = 0
        if action == 0:
            x = 1
        elif action == 1:
            x = -1
        elif action == 2:
            x = 0
        elif action == 3:
            y = 1
        elif action == 4:
            y = -1
        elif action == 5:
            y = 0

        for map in Life.worldMap:
            if map.x == self.x and map.y == self.y:
                if map.getName() == 's':
                    self.health += map.health
                    Life.toDelete.append(map)
                    break
                if map.getName() == 'r':
                    x = 0
                    y = 0
                if map.getName() == 'w':
                    self.health -= 1

        if self.health > 9:
            self.health = 5
            Life.newWorldMap.append(Wolf(self.x, self.y, self.health))

        if random.randint(0, 10) == 0:
            self.health -= 1

        if self.x + x > GeneratorWorld.width or self.x + x < 0:
            x = 0
        if self.y + y > GeneratorWorld.height or self.y + y < 0:
            y = 0

        Life.newWorldMap.append(Wolf(self.x + x, self.y + y, self.health))

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getName(self):
        return 'e'

    def getHealth(self):
        return self.health