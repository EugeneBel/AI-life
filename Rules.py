import os
import random

import Life

#ttttf:ation:score

rules_sven = []
rules_wolf = []

fitness_sven_offset = 0
fitness_wolf_offset = 0

max_rules = 100


def load_rules():
    if not os.path.exists('sven'):
        sven_create = open('sven', 'w')
        sven_create.close()
    if not os.path.exists('wolf'):
        sven_create = open('wolf', 'w')
        sven_create.close()
    sven_f = open('sven', 'r')
    sven_f_rules = sven_f.read()
    sven_f.close()
    for r in sven_f_rules.split('\n'):
        if r.strip() != '':
            rules_sven.append(r)
    wolf_f = open('wolf', 'r')
    wolf_f_rules = wolf_f.read()
    wolf_f.close()
    for r in wolf_f_rules.split('\n'):
        if r.strip() != '':
            rules_wolf.append(r)


def get_cell(x, y):
    for i in Life.worldMap:
        if i.getX() == x and i.getY() == y:
            return i
    return None


def get_neighbors(x, y):
    return [
        get_cell(x - 1, y),
        get_cell(x + 1, y),
        get_cell(x, y + 1),
        get_cell(x, y - 1)
    ]


def form_condition_str(x, y):
    neighbors = get_neighbors(x, y)
    self_cell = get_cell(x, y)
    result_str = ''
    for n in neighbors:
        if n is None:
            result_str += ' '
        else:
            result_str += n.getName()
    result_str += str(self_cell.getHealth())
    return result_str


def legacy_condition_find(condition, collection):
    score_max = 0
    result = None
    for i in collection:
        mas = i.split(':')
        if mas[0] == condition:
            score_now = int(mas[2])
            if score_max < score_now:
                result = i
                score_max = score_now
    return result

def condition_find(condition, collection):
    sum = 0
    result = None
    for i in collection:
        mas = i.split(':')
        if mas[0] == condition:
            score_now = int(mas[2])
            sum += score_now
    if sum == 0:
        return None
    obr_koef = 1 / sum
    local_conditial = []
    last = 0
    for i in collection:
        p = int(i.split(':')[2]) * obr_koef * 10000 + last
        last = p
        local_conditial.append(i + ":" + str(p))
    r = random.randint(0, 10000)
    last_bound = 0
    for i in local_conditial:
        pv = int(round(float(i.split(':')[3])))
        if r >= last_bound and pv < r:
            result = i.split(':')[0] + ":" + i.split(':')[1] + ":" + i.split(':')[2]
        last_bound = pv
    if result is None:
        return legacy_condition_find(condition, collection)
    return result


def get_score_from_position(x, y, action):
    if action == 0:
        return str(get_score(get_cell(x, y), get_cell(x + 1, y)))
    elif action == 1:
        return str(get_score(get_cell(x, y), get_cell(x - 1, y)))
    elif action == 2:
        return str(get_score(get_cell(x, y), get_cell(x, y)))
    elif action == 3:
        return str(get_score(get_cell(x, y), get_cell(x, y + 1)))
    elif action == 4:
        return str(get_score(get_cell(x, y), get_cell(x, y - 1)))
    elif action == 5:
        return str(get_score(get_cell(x, y), get_cell(x, y)))


def gen_new_rules(x, y, collection):
    result = form_condition_str(x, y)
    action = random.randint(0, 5)
    result += ":" + str(action)
    result += ":" + get_score_from_position(x, y, action)
    if len(collection) <= max_rules and int(result.split(':')[2]) != 0:
        collection.append(result)
    return result.split(':')[1]


def update_score(rule, offset, collection):
    collection.remove(rule)
    part_rule = rule.split(':')
    if int(part_rule[2]) > 1000:
        part_rule[2] = '1000'
    if int(part_rule[2]) < -1000:
        part_rule[2] = '-1000'
    collection.insert(0, part_rule[0] + ':' + part_rule[1] + ':' + str(int(part_rule[2]) + int(offset)))


def _find_action_(x, y, collection):
    condition = condition_find(form_condition_str(x, y), collection)
    if condition is None:
        return gen_new_rules(x, y, collection)
    else:
        action = condition.split(':')[1]
        offset_score = get_score_from_position(x, y, int(action))
        update_score(condition, offset_score, collection)
        return action


def get_score(cell_original, cell_next):
    if cell_original.getName() == 's' and cell_next is None:
        return 0
    elif cell_original.getName() == 'e' and cell_next is None:
        return 0
    elif cell_original.getName() == 's' and cell_next.getName() == 'e':
        return -10
    elif cell_original.getName() == 's' and cell_next.getName() == 'g':
        return 10
    elif cell_original.getName() == 's' and cell_next.getName() == 'r':
        return -1
    elif cell_original.getName() == 's' and cell_next.getName() == 'w':
        return 1
    elif cell_original.getName() == 'e' and cell_next.getName() == 's':
        return 10
    elif cell_original.getName() == 'e' and cell_next.getName() == 'w':
        return -10
    elif cell_original.getName() == 'e' and cell_next.getName() == 'r':
        return -1
    elif cell_original.getName() == 'e' and cell_next.getName() == 'g':
        return 1
    return 0


def get_action(x, y, cell):
    if cell == 'sven':
        return _find_action_(x, y, rules_sven)
    elif cell == 'wolf':
        return _find_action_(x, y, rules_wolf)


def mutation(collection):
    max = -1
    for r in collection:
        s = int(r.split(':')[2])
        if s > max:
            max = s
    for r in collection:
        part = r.split(':')
        if max // 4 < int(part[2]):
            collection.remove(r)
            action = random.randint(0, 5)
            collection.insert(0, part[0] + ':' + str(action) + ':' + part[2])
    for r in collection:
        part = r.split(':')
        if max // 3 < int(part[2]):
            collection.remove(r)
            pos = random.randint(1, len(part[0])-2)
            new_cond = random.randint(0, 5)
            liter = ' '
            if new_cond == 1:
                liter = 'e'
            elif new_cond == 2:
                liter = 's'
            elif new_cond == 3:
                liter = 'w'
            elif new_cond == 4:
                liter = 'r'
            elif new_cond == 5:
                liter = 'g'
            condition = part[0][:pos-1] + liter + part[0][pos:]
            collection.insert(0, condition + ':' + part[1] + ':' + part[2])


def stage_end():
    mutation(rules_sven)
    mutation(rules_wolf)
    sven_r = open('sven', 'w')
    for r in rules_sven:
        sven_r.write(r + '\n')
    sven_r.close()
    wolf_r = open('wolf', 'w')
    for r in rules_wolf:
        wolf_r.write(r + '\n')
    wolf_r.close()


def get_fitness(collection, offset):
    fitness = offset
    for x in collection:
        fitness += int(x.split(':')[2])
    return fitness