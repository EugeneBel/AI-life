# AI-life
Сellular automaton simulate life sven and wolf in one world.  
Automaton find rules for wolf and sven through genetic algorithms.
## Run
In terminal type command:


Run and visualise process without record.  
Use key in keyboard:  
q - enable/disable draw process  
s - start record  
w - load last record
arrows - move cam
minus and plus un num - change zoom
```python 
python3 Main.py
```
Require pygame  

Run calc rules for sven and wolf without record and GUI.
```python
python3 MainNoGraphics.py
```

Run calc rules for sven and wolf with record in 'replay' file.
```python
python3 MainNoGraphicsRecord.py
```

Record will stop on close program.

## Draw
In file Draw.py change size window.
```python
size = [800, 600]
```
Size 800x600 is default


## Generator world
In file GeneratorWorld.py stored constants for generator.  

Set size world
```python
width
height
```

All probability set in units. (1 units = 0,1%)

Probability spawn wolf in cell.
```python
p_wolf
```
Default: 5

Probability spaw sven in cell.
```python
p_sven
```
Default: 5

Probability start river in cell.
```python
p_river
```
Default: 10

Probability start rock in cell.
```python
p_rock
```
Default: 3

Probability start grass in cell.
```python
p_grass
```