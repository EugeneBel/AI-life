import random

import GeneratorWorld
import Life
from Cell import Cell

class Grass(Cell):

    def __init__(self, x, y, health):
        self.x = x
        self.y = y
        self.health = health

    def update(self, tick):
        if tick % 10 == 0 and random.randint(0, 100) <= 30:
            x = random.randint(-1, 1)
            y = random.randint(-1, 1)
            ox = self.x + x
            oy = self.y + y
            if not Life.obj_exist(ox, oy) and ox >= 0 and ox <= GeneratorWorld.width and oy >= 0 and oy <= GeneratorWorld.height:
                Life.newWorldMap.append(Grass(self.x + x, self.y + y, self.health))
        Life.newWorldMap.append(Grass(self.x, self.y, self.health))

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getName(self):
        return 'g'

    def getHealth(self):
        return self.health