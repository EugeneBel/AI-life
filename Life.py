from Cell import Cell

worldMap = [Cell()]

newWorldMap = [Cell()]

toDelete = []

worldMap.clear()
newWorldMap.clear()

def obj_delete(x, y):
    for cell in worldMap:
        if cell.getX() == x and cell.getY() == y:
            worldMap.remove(cell)
            break


def obj_exist(x, y):
    find = False
    for cell in worldMap:
        if cell.getX() == x and cell.getY() == y:
            find = True
    return find


def can_add(cell):
    for c in toDelete:
        if c.getX() == cell.getX() and c.getY() == cell.getY() and c.getName() == cell.getName():
            return False
    return True


def life(tick):
    alive_sven = False
    alive_wolf = False
    toDelete.clear()
    newWorldMap.clear()
    for cell in worldMap:
        cell.update(tick)
        if cell.getName() == 's':
            alive_sven = True
        if cell.getName() == 'e':
            alive_wolf = True
    worldMap.clear()
    for cell in newWorldMap:
        if can_add(cell):
            worldMap.append(cell)
    return alive_sven and alive_wolf