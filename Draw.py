import GeneratorWorld
import Life
import pygame

import Rules
from Grass import Grass
from Rock import Rock
from Sven import Sven
from Water import Water
from Wolf import Wolf

pygame.init()

background = [85, 50, 0]
black = [0, 0, 0]
white = [255, 255, 255]
gray = [125, 125, 125]
red = [255, 0, 0]
green = [0, 255, 0]
blue = [0, 0, 255]
wgray = [217, 217, 217]
draw = True
replayer = False
record = False

tick = 1
life = 1
zoom = 10
fitness_wolf = 0
fitness_sven = 0

cam_x = 0
cam_y = 0

screen_width = 800
screen_height = 590

size = [screen_width, screen_height + 10]
screen = pygame.display.set_mode(size)

pygame.display.set_caption("Life")

run = True
clock = pygame.time.Clock()

key_dict = {pygame.K_UP: False,
            pygame.K_DOWN: False,
            pygame.K_RIGHT: False,
            pygame.K_LEFT: False,
            pygame.K_KP_PLUS: False,
            pygame.K_KP_MINUS: False,
            }

Rules.load_rules()

record_file = None
buffer_file = ""
record_cap = ""


def click_on_bar(pos, current_tick):
    if screen_height - pos[1] <= 10:
        return int(round(len(parts) * (pos[0] / screen_width)))
    return current_tick


while run:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                draw = not draw
            elif event.key == pygame.K_r:
                GeneratorWorld.gen_new_map()
                tick = 0
            elif event.key == pygame.K_w:
                record = False
                replayer = True
                record_file = open("replay", 'r')
                buffer_file = record_file.read()
                parts = buffer_file.split('\n')
            elif event.key == pygame.K_s:
                record_file = open("replay", 'w')
                record_cap = " Record"
                record = True
            else:
                key_dict[event.key] = True
        if event.type == pygame.KEYUP:
            key_dict[event.key] = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            tick = click_on_bar(pygame.mouse.get_pos(), tick)

    if not replayer:

        fitness_wolf = Rules.get_fitness(Rules.rules_wolf, Rules.fitness_wolf_offset)
        fitness_sven = Rules.get_fitness(Rules.rules_sven, Rules.fitness_sven_offset)
        pygame.display.set_caption("Life: " + str(life) + "  Tick: " + str(tick) + " cam: "
                                   + str(-cam_x) + ":" + str(-cam_y) + "  zoom: " + str(zoom)
                                   + "  fitness -  W: " + str(fitness_wolf) + "  S: " + str(fitness_sven) + record_cap)

        if not Life.life(tick):
            Rules.fitness_sven_offset = 0
            Rules.fitness_wolf_offset = 0
            Rules.stage_end()
            if not record:
                GeneratorWorld.gen_new_map()
            else:
                run = False
            tick = 0
            life += 1
        tick += 1

        if record:
            to_flush = ""
            for x in Life.worldMap:
                to_flush += str(x.getName()) + ":" + str(x.getX()) + ":" + str(x.getY()) + ","
            to_flush += "\n"
            record_file.write(to_flush)

        if not draw:
            continue

    clock.tick(30)

    if replayer:

        pygame.display.set_caption("Life: It is Record | Tick: " + str(tick) + "/" + str(len(parts)) + " cam: "
                                   + str(-cam_x) + ":" + str(-cam_y) + "  zoom: " + str(zoom))

        if tick >= len(parts):
            run = False
            continue
        objects = parts[tick].split(',')
        Life.worldMap.clear()
        for j in objects:
            params = j.split(':')
            if params[0] == 'e':
                Life.worldMap.append(Wolf(int(params[1]), int(params[2]), 1))
            if params[0] == 'w':
                Life.worldMap.append(Water(int(params[1]), int(params[2])))
            if params[0] == 's':
                Life.worldMap.append(Sven(int(params[1]), int(params[2]), 1))
            if params[0] == 'g':
                Life.worldMap.append(Grass(int(params[1]), int(params[2]), 1))
            if params[0] == 'r':
                Life.worldMap.append(Rock(int(params[1]), int(params[2])))
        tick += 1

    if key_dict[pygame.K_LEFT]:
        cam_x += 1
    if key_dict[pygame.K_RIGHT]:
        cam_x -= 1
    if key_dict[pygame.K_UP]:
        cam_y += 1
    if key_dict[pygame.K_DOWN]:
        cam_y -= 1
    if key_dict[pygame.K_KP_PLUS]:
        zoom += 1
    if key_dict[pygame.K_KP_MINUS]:
        if (zoom - 1) > 0:
            zoom -= 1

    screen.fill(background)

    for cell in Life.worldMap:
        x = cell.getX() * zoom + cam_x * zoom
        y = cell.getY() * zoom + cam_y * zoom
        if isinstance(cell, Rock):
            pygame.draw.rect(screen, gray, [x, y, zoom, zoom], 0)
        if isinstance(cell, Sven):
            pygame.draw.rect(screen, white, [x, y, zoom, zoom], 0)
        if isinstance(cell, Water):
            pygame.draw.rect(screen, blue, [x, y, zoom, zoom], 0)
        if isinstance(cell, Wolf):
            pygame.draw.rect(screen, red, [x, y, zoom, zoom], 0)
        if isinstance(cell, Grass):
            pygame.draw.rect(screen, green, [x, y, zoom, zoom], 0)

    if replayer:
        pygame.draw.rect(screen, wgray, [0, screen_height, screen_width, 10], 0)
        pygame.draw.rect(screen, black, [screen_width * tick // len(parts), screen_height, 10, 10], 0)

    pygame.display.flip()

Rules.stage_end()
record_file.close()
pygame.quit()